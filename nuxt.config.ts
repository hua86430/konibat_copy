import { pwa } from './config/pwa';
import { appDescription } from './constants/index';

export default defineNuxtConfig({
    modules: ['@vueuse/nuxt', '@unocss/nuxt', '@pinia/nuxt', '@vite-pwa/nuxt', '@element-plus/nuxt'],

    experimental: {
        // when using generate, payload js assets included in sw precache manifest
        // but missing on offline, disabling extraction it until fixed
        payloadExtraction: false,
        inlineSSRStyles: false,
        renderJsonPayloads: true
    },

    css: ['@unocss/reset/tailwind.css'],

    elementPlus: {
        icon: 'ElIcon',
        importStyle: 'scss'
    },
    nitro: {
        esbuild: {
            options: {
                target: 'esnext'
            }
        },
        prerender: {
            crawlLinks: false,
            routes: ['/'],
            ignore: ['/hi']
        }
    },

    app: {
        head: {
            viewport: 'width=device-width,initial-scale=1',
            link: [
                { rel: 'icon', href: '/favicon.ico', sizes: 'any' },
                { rel: 'icon', type: 'image/svg+xml', href: '/nuxt.svg' },
                { rel: 'apple-touch-icon', href: '/apple-touch-icon.png' },
                { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css' }
            ],
            meta: [
                { name: 'viewport', content: 'width=device-width, initial-scale=1' },
                { name: 'description', content: appDescription },
                {
                    name: 'apple-mobile-web-app-status-bar-style',
                    content: 'black-translucent'
                }
            ]
        }
    },

    pwa,

    devtools: {
        enabled: false
    }
});
