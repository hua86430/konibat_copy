﻿export interface INavigationList {
    label: string;
    link: string;
}
