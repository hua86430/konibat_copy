﻿export interface ISpecialItem {
    redirectUrl: string;
    imgUrl: string;
}
