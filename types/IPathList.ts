﻿export interface IPathList {
    path: string;
    pathName: string;
}
