﻿export interface INewsItem {
    imgUrl: string;
    title: string;
    subTitle: string;
    date: string;
    redirectUrl: string;
}
